package view;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

import java.util.Timer;

/**
 * view of minesweeperish
 * creates an instance of gridMap
 * creates panes and nodes for display
 */
public class View extends Application {
    GridMap gridMap;
    BorderPane mainPane = new BorderPane();
    HBox toolbar = new HBox();
    Button startBtn = new Button("start");
    Timer timer;
    DoubleProperty seconds;
    Text bombsLeft;
    Text bombCount;
    Text time;
    Text timeCount;

    /**
     * sets up display with bomb count, timer, and start button
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        seconds = new SimpleDoubleProperty(0);
        gridMap = new GridMap();
        timer = new Timer();
        timeCount = new Text("" + gridMap.seconds);
        time = new Text("time: \n");
        bombsLeft = new Text("bombs left: \n");
        bombCount = new Text("" + gridMap.bombCt);
        Bindings.bindBidirectional(bombCount.textProperty(), gridMap.bombCt, new NumberStringConverter());
        Bindings.bindBidirectional(timeCount.textProperty(), gridMap.seconds, new NumberStringConverter());

        toolbar.getChildren().addAll(bombsLeft, bombCount, startBtn, time, timeCount);
        toolbar.setSpacing(50);
        toolbar.setPadding(new Insets(5, 5, 5, 5));
        toolbar.setAlignment(Pos.CENTER);

        gridMap.setAlignment(Pos.CENTER);
        mainPane.setTop(toolbar);
        mainPane.setCenter(gridMap);

        Scene scene = new Scene(mainPane, 540, 565);

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

        /**
         * handles when start button is clicked
         */
        startBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                GridMap gridMap = new GridMap();
                toolbar.getChildren().removeAll(bombsLeft, bombCount, startBtn, time, timeCount);
                bombsLeft = new Text("bombs left: \n");
                bombCount = new Text("" + gridMap.bombCt);
                timeCount = new Text("" + gridMap.seconds);
                time = new Text("time: \n");
                Bindings.bindBidirectional(bombCount.textProperty(), gridMap.bombCt, new NumberStringConverter());
                Bindings.bindBidirectional(timeCount.textProperty(), gridMap.seconds, new NumberStringConverter());
                gridMap.setAlignment(Pos.CENTER);
                mainPane.setCenter(gridMap);
                toolbar.getChildren().addAll(bombsLeft, bombCount, startBtn, time, timeCount);
            }
        });
    }
}

