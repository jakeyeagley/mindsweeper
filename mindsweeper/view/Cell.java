package view;

/**
 * Created by jakeyeagley on 11/21/16.
 *
 * cell is a custom button for minesweeper
 */
public class Cell extends CustomButton{
    /**
     * isBomb is true if cell a bomb
     * isFlagged is true if cell is flagged
     * isQuestion is true if cell is marked as question mark
     * isNothing is true if the cell is not touching any bombs
     * xPos is cells position in the x direction
     * yPos is cells position in the y direction
     */
    public Boolean isBomb;
    public Boolean isFlagged;
    public Boolean isQuestion;
    public Boolean isMarkedZero;
    public Boolean isNothing;
    int xPos = 0;
    int yPos = 0;

    /**
     * constructor
     * sets all booleans to false and gives space to insert a number
     */
    public Cell() {
        this.setText("  ");
        this.isBomb = false;
        this.isFlagged = false;
        this.isQuestion = false;
        this.isMarkedZero = false;
        this.isNothing = false;
    }

    /**
     * setter functions
     */
    public void setxPos(int x) {this.xPos = x;}
    public void setyPos(int y) {this.yPos = y;}

    public void setBomb(){
        this.isBomb = true;
    }
    public void setFlag() { this.isFlagged = true; }
    public void unSetFlag() { this.isFlagged = false; }
    public void setQuestion() { this.isQuestion = true; }
    public void unSetQuestion() { this.isQuestion = false; }
    public void setMarkAsZero() {this.isMarkedZero = true; }

    /**
     * display functions
     */
    public void markIsBomb(){
        this.setDisable(true);
        this.setCSS3();
    }

    public void markIsNothing(){
        this.setDisable(true);
        this.setCSS1();
        this.isNothing = true;
    }

    public void markFlag(){
        this.setText("X");
        this.setDisabled(true);
        setFlag();
    }

    public void markQues(){
        this.setText("?");
        this.setDisabled(false);
        setQuestion();
        unSetFlag();
    }

    public void unSet(){
        this.setText("  ");
        unSetFlag();
        unSetQuestion();
    }

}

