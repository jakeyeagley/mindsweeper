package view;

import controller.Controller;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by jakeyeagley on 11/22/16.
 */
public class GridMap extends GridPane {
    /**
     * games status
     * 0 = on going
     * 1 = win
     * 2 = lost
     * -1 = paused
     *
     * x and y are size of grid
     * bombCt is binded to view to update the count of bombs
     * seconds is binded to view to update the time
     * timer keeps track of the time a player has been playing
     * list is single array of cells that is put into a double array after being suffled
     * map is a 2D array to keep track of positions of cells in the grid
     */
    static final int x = 20;
    static final int y = 20;

    public DoubleProperty bombCt;
    int status = -1;
    DoubleProperty seconds;
    Timer timer;

    ArrayList<Cell> list = new ArrayList<Cell>();
    static Cell[][] map = new Cell[x][y];

    /**
     * constructor
     * handles grid when clicked on both primary click and secondary click
     * sets cells according to position and bomb status
     * starts and ends timer according to status
     * tells user when they win or lose
     */
    public GridMap() {
        bombCt = new SimpleDoubleProperty(x*y/20); //change bomb count here
        seconds = new SimpleDoubleProperty(0);
        for (int i = 0; i < (x * y); i++) {
            Cell cell = new Cell();
            if (i < bombCt.getValue())
                cell.setBomb();

            list.add(cell);
        }

        Collections.shuffle(list);

        int index = 0;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                map[i][j] = list.get(index);
                map[i][j].setxPos(j);
                map[i][j].setyPos(i);
                index++;

                setConstraints(map[i][j], i, j);
                this.getChildren().add(map[i][j]);
            }
        }
        Controller controller = new Controller(map,x,y);

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                int cellX = i;
                int cellY = j;
                map[i][j].setOnMousePressed(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if(event.isPrimaryButtonDown() && !map[cellX][cellY].isFlagged) {
                            status = controller.checkPrimary(cellX, cellY, seconds);
                            if(status == 1){
                                double finalTime = seconds.getValue();
                                int finalTime1 = (int)finalTime;
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setHeaderText("You win");
                                alert.setContentText("time: " + finalTime1);
                                alert.showAndWait();
                            }
                            else if(status == 2){
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setHeaderText("Game Over");
                                alert.setContentText("You Lose");
                                alert.showAndWait();
                            }
                        }
                        if(event.isSecondaryButtonDown()){
                            if(!map[cellX][cellY].isFlagged && !map[cellX][cellY].isQuestion) {
                                map[cellX][cellY].markFlag();
                                bombCt.set(bombCt.getValue() - 1);
                            }
                            else if(map[cellX][cellY].isFlagged && !map[cellX][cellY].isQuestion) {
                                map[cellX][cellY].markQues();
                                bombCt.set(bombCt.getValue() + 1);
                            }
                            else if(map[cellX][cellY].isQuestion && !map[cellX][cellY].isFlagged)
                                map[cellX][cellY].unSet();
                        }
                    }
                });
            }
        }
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (status == 0) {
                    seconds.set(seconds.getValue() + 1);
                }
                else if(status == 1 || status == 2 || status == -1)
                    seconds.set(0);
            }
        }, 0, 500);
        this.setScaleShape(true);
    }
}

