package view;

import javafx.scene.control.Button;

/**
 * Created by jakeyeagley on 11/23/16.
 * custom button for minesweeperish
 */
public class CustomButton extends Button {
    public CustomButton() {
        this.getStylesheets().addAll("view/custom.css");
    }

    public CustomButton(String s) {
        this();
        this.setText(s);
    }

    public void setCSS1() { this.getStylesheets().clear(); }

    public void setCSS2() {
        this.setDisable(true);
    }

    public void setCSS3() {
        this.setId("special-button");
    }

    public void setCSS4() {
        this.setId("lame-button");
    }
}
