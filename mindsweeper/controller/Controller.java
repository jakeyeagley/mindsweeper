package controller;

import javafx.beans.property.DoubleProperty;
import javafx.scene.control.Alert;
import view.Cell;

/**
 * Created by jakeyeagley on 11/22/16.
 */
public class Controller {
    /**
     * status
     * 0 = going
     * 1 = win
     * 2 = lose
     * -1 = pause
     *
     * cellX and Y for location of cell
     * mapX and Y for size of grid
     *
     * double array of cells from gridMap
     */
    int cellX = 0;
    int cellY = 0;
    int mapX = 0;
    int mapY = 0;
    int status = -1;

    Cell[][] map;

    /**
     * constructor
     * @param map
     * @param x
     * @param y
     */
    public Controller(Cell[][] map, int x, int y) {
        this.map = map;
        this.mapX = x;
        this.mapY = y;
    }

    /**
     * checking left click from the gridMap
     * takes the position and the time
     * if is a bomb marks all as bombs and ends game
     * if no bombs are surrounding the cell opens all cells around it
     * keeps track of the status of game and gives it to the gridMap
     * @param x
     * @param y
     * @param seconds
     * @return status of game
     */
    public int checkPrimary(int x, int y, DoubleProperty seconds) {
        this.cellX = x;
        this.cellY = y;

        if (map[x][y].isBomb) {
            status = 2;

            map[x][y].markIsBomb();
            for (int i = 0; i < mapX; i++) {
                for (int j = 0; j < mapY; j++) {
                    if (map[i][j].isBomb)
                        map[i][j].markIsBomb();
                }
            }
            for (int i = 0; i < mapX; i++) {
                for (int j = 0; j < mapY; j++) {
                    if(!map[i][j].isBomb)
                        map[i][j].setDisable(true);
                }
            }
            return status;
        }
        else {
            if(bombsNextTo(x,y) == 0)
                setSurronding(x,y);
            else
                map[x][y].setText("" + bombsNextTo(x, y));
            if(checkGameOver())
            {
                status = 1;
                return status;
            }
        }
        status = 0;
        return status;
    }

    /**
     * sets the number of bombs surrounding the opened cell
     * if a cell next to opened cell isn't touching a bomb then all cells
     * with zero bombs surrounding it will also be opened
     * @param x
     * @param y
     */
    public void setSurronding(int x, int y) {

        if (bombsNextTo(x, y) != 0)
            return;
        else {
            map[x][y].markIsNothing();
            map[x][y].setMarkAsZero();

            try {
                map[x][y].textProperty().set(Integer.toString(bombsNextTo(x, y)));
            } catch (ArrayIndexOutOfBoundsException e) {}
            try {
                map[x + 1][y].textProperty().set(Integer.toString(bombsNextTo(x, y)));
            } catch (ArrayIndexOutOfBoundsException e) {}
            try {
                map[x + 1][y - 1].textProperty().set(Integer.toString(bombsNextTo(x + 1, y - 1)));
            } catch (ArrayIndexOutOfBoundsException e) {}
            try {
                map[x + 1][y + 1].textProperty().set(Integer.toString(bombsNextTo(x + 1, y + 1)));
            } catch (ArrayIndexOutOfBoundsException e) {}
            try {
                map[x][y + 1].textProperty().set(Integer.toString(bombsNextTo(x, y + 1)));
            } catch (ArrayIndexOutOfBoundsException e) {}
            try {
                map[x - 1][y].textProperty().set(Integer.toString(bombsNextTo(x - 1, y)));
            } catch (ArrayIndexOutOfBoundsException e) {}
            try {
                map[x - 1][y + 1].textProperty().set(Integer.toString(bombsNextTo(x - 1, y + 1)));
            } catch (ArrayIndexOutOfBoundsException e) {}
            try {
                map[x - 1][y - 1].textProperty().set(Integer.toString(bombsNextTo(x - 1, y - 1)));
            } catch (ArrayIndexOutOfBoundsException e) {}
            try {
                map[x][y - 1].textProperty().set(Integer.toString(bombsNextTo(x, y - 1)));
            } catch (ArrayIndexOutOfBoundsException e) {}

            try{
            if (bombsNextTo(x + 1, y) == 0 && !map[x + 1][y].isMarkedZero)
                setSurronding(x + 1, y);
            } catch (ArrayIndexOutOfBoundsException e) {}
            try{
            if (bombsNextTo(x - 1, y) == 0 && !map[x - 1][y].isMarkedZero)
                setSurronding(x - 1, y);
            } catch (ArrayIndexOutOfBoundsException e) {}
            try{
            if (bombsNextTo(x, y + 1) == 0 && !map[x][y + 1].isMarkedZero)
                setSurronding(x, y + 1);
            } catch (ArrayIndexOutOfBoundsException e) {}
            try{
            if (bombsNextTo(x + 1, y + 1) == 0 && !map[x + 1][y + 1].isMarkedZero)
                setSurronding(x + 1, y + 1);
            } catch (ArrayIndexOutOfBoundsException e) {}
            try{
            if (bombsNextTo(x + 1, y - 1) == 0 && !map[x + 1][y - 1].isMarkedZero)
                setSurronding(x + 1, y - 1);
            } catch (ArrayIndexOutOfBoundsException e) {}
            try{
            if (bombsNextTo(x - 1, y - 1) == 0 && !map[x - 1][y - 1].isMarkedZero)
                setSurronding(x - 1, y - 1);
            } catch (ArrayIndexOutOfBoundsException e) {}
            try{
            if (bombsNextTo(x, y - 1) == 0 && !map[x][y - 1].isMarkedZero)
                setSurronding(x, y - 1);
            } catch (ArrayIndexOutOfBoundsException e) {}
            try{
            if (bombsNextTo(x - 1, y + 1) == 0 && !map[x - 1][y + 1].isMarkedZero)
                setSurronding(x - 1, y + 1);
            } catch (ArrayIndexOutOfBoundsException e) {}
        }
    }

    /**
     * sets the number of bombs nexted to a selected cell
     * @param x
     * @param y
     * @return number of bombs surrounding the selected cell
     */
    public int bombsNextTo(int x,int y) {
        int bombsNextTo = 8;

        map[x][y].markIsNothing();

        try {
            if (!map[x + 1][y].isBomb) {bombsNextTo--;}
        }catch (ArrayIndexOutOfBoundsException e){bombsNextTo--;}
        try {
            if (!map[x][y + 1].isBomb) { bombsNextTo--; }
        }catch (ArrayIndexOutOfBoundsException e){bombsNextTo--;}
        try {
            if (!map[x - 1][y].isBomb) { bombsNextTo--; }
        }catch (ArrayIndexOutOfBoundsException e){bombsNextTo--;}
        try {
            if (!map[x][y - 1].isBomb) { bombsNextTo--; }
        }catch (ArrayIndexOutOfBoundsException e){bombsNextTo--;}
        try {
            if (!map[x + 1][y + 1].isBomb) { bombsNextTo--; }
        }catch (ArrayIndexOutOfBoundsException e){bombsNextTo--;}
        try {
            if (!map[x + 1][y - 1].isBomb) { bombsNextTo--; }
        }catch (ArrayIndexOutOfBoundsException e){bombsNextTo--;}
        try {
            if (!map[x - 1][y + 1].isBomb) { bombsNextTo--; }
        }catch (ArrayIndexOutOfBoundsException e){bombsNextTo--;}
        try {
            if (!map[x - 1][y - 1].isBomb) { bombsNextTo--; }
        }catch (ArrayIndexOutOfBoundsException e){bombsNextTo--;}

        return bombsNextTo;
    }

    /**
     * checks to see if player has won
     * @return true if game is over and the player has won
     */
    public boolean checkGameOver() {
        for (int i = 0; i < mapX; i++) {
            for (int j = 0; j < mapY; j++) {
                if(!map[i][j].isNothing && !map[i][j].isBomb){
                    return false;
                }
            }
        }
        return true;
    }
}

